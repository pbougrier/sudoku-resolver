import { Resolver } from './Sudoku'


it('gives the array, the column, the square containing our value', () => {
  const r = new Resolver()
  expect(r.cells[0].line   ).toEqual([ 0,  1,  2,  3,  4,  5,  6,  7,  8])
  expect(r.cells[80].column).toEqual([ 8, 17, 26, 35, 44, 53, 62, 71, 80])
  expect(r.cells[39].square).toEqual([30, 31, 32, 39, 40, 41, 48, 49, 50])
})

it('gives next legal value', () => {
  const r = new Resolver()
  r.cells[8].value = 2
  r.cells[9].value = 4
  r.cells[20].value = 6
  r.cells[7].value = 8
  
  const c = r.cells[0]
  expect(c.nextLegalValue()).toBe(1); c.value = 1
  expect(c.nextLegalValue()).toBe(3); c.value = 3
  expect(c.nextLegalValue()).toBe(5); c.value = 5
  expect(c.nextLegalValue()).toBe(7); c.value = 7
  expect(c.nextLegalValue()).toBe(9); c.value = 9
  expect(c.nextLegalValue()).toBe(0)
})

it('is locked when non-zero value is set', () => {
  const c = new Resolver().cells[0]
  expect(c.locked).toBeFalsy()
  c.set(1)
  expect(c.value).toBe(1)
  expect(c.locked).toBeTruthy()
  c.set(0)
  expect(c.locked).toBeFalsy()
})


/*
import { Grid } from './Sudoku'

const g = new Grid(2)
const gg = new Grid(3)

it('Gives the first cell id of the row', () => {
  expect(g.rowId(0)).toBe(0)
  expect(g.rowId(3)).toBe(0)
  expect(g.rowId(12)).toBe(12)
  expect(g.rowId(15)).toBe(12)
})

it('Gives the top cell id of the column', () => {
  expect(g.colId(0)).toBe(0)
  expect(g.colId(12)).toBe(0)
  expect(g.colId(11)).toBe(3)
})

it('Visit all the cells from a row', () => {
  const check = [4, 5, 6, 7]
  g.visitLineFor(6, (i,v) => {
    expect(i).toBe(check.shift())
  })
  expect(check.length).toBe(0)
})

it('Visit all the cells from a column', () => {
  const check = [2, 6, 10, 14]
  g.visitColumnFor(6, (i,v) => {
    expect(i).toBe(check.shift())
  })
  expect(check.length).toBe(0)
})

it('Visit all the cells from a square', () => {
  const check = [2, 3, 6, 7]
  g.visitSquareFor(6, (i,v) => {
    expect(i).toBe(check.shift())
  })
  expect(check.length).toBe(0)
})

it('Visit all the cells from a square (harder)', () => {
  const check = [6, 7, 8, 15, 16, 17, 24, 25, 26]
  gg.visitSquareFor(25, (i,v) => {
    expect(i).toBe(check.shift())
  })
  expect(check.length).toBe(0)
})

it('Resolve an empty sudoku', () => {
  const r = new Grid(2)
  expect(r.resolve()).toBe(true)
  expect(r.cells).toEqual([1,2,3,4, 3,4,1,2, 2,1,4,3, 4,3,2,1])
})

it('Resolve2 an empty sudoku', () => {
  const r = new Grid(2)
  r.initResolve2()
  while(!r.resolve2()) {}
  expect(r.cells).toEqual([1,2,3,4, 3,4,1,2, 2,1,4,3, 4,3,2,1])
})


it('Resolve a sudoku with locked cells', () => {
  const r = new Grid(2)
  r.set(0, 4)
  r.lock(0)
  r.set(12, 1)
  r.lock(12)
  expect(r.resolve()).toBe(true)
  expect(r.cells).toEqual([4,1,2,3, 2,3,1,4, 3,2,4,1, 1,4,3,2])
})

it('Resolve2 a sudoku with locked cells', () => {
  const r = new Grid(2)
  r.set(0, 4)
  r.lock(0)
  r.set(12, 1)
  r.lock(12)
  r.initResolve2()
  while (!r.resolve2()) {}
  expect(r.cells).toEqual([4,1,2,3, 2,3,1,4, 3,2,4,1, 1,4,3,2])
})


it('Fail to resolve a sudoku with bad locked cells', () => {
  const r = new Grid(2)
  r.set(0, 4)
  r.lock(0)
  r.set(12, 4)
  r.lock(12)
  expect(r.resolve()).toBeFalsy()
  expect(r.cells).toEqual([4,0,0,0, 0,0,0,0, 0,0,0,0, 4,0,0,0])
})
*/