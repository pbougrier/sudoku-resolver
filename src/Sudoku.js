
class Resolver {
    
    constructor() {
        this.cells = Array(81)
        
        this.lines = [
            [ 0,  1,  2,  3,  4,  5,  6,  7,  8], [ 9, 10, 11, 12, 13, 14, 15, 16, 17], [18, 19, 20, 21, 22, 23, 24, 25, 26],
            [27, 28, 29, 30, 31, 32, 33, 34, 35], [36, 37, 38, 39, 40, 41, 42, 43, 44], [45, 46, 47, 48, 49, 50, 51, 52, 53],
            [54, 55, 56, 57, 58, 59, 60, 61, 62], [63, 64, 65, 66, 67, 68, 69, 70, 71], [72, 73, 74, 75, 76, 77, 78, 79, 80]]
        
        this.columns = [
            [ 0,  9, 18, 27, 36, 45, 54, 63, 72], [ 1, 10, 19, 28, 37, 46, 55, 64, 73], [ 2, 11, 20, 29, 38, 47, 56, 65, 74],
            [ 3, 12, 21, 30, 39, 48, 57, 66, 75], [ 4, 13, 22, 31, 40, 49, 58, 67, 76], [ 5, 14, 23, 32, 41, 50, 59, 68, 77],
            [ 6, 15, 24, 33, 42, 51, 60, 69, 78], [ 7, 16, 25, 34, 43, 52, 61, 70, 79], [ 8, 17, 26, 35, 44, 53, 62, 71, 80]]
        
        this.squares = [
            [ 0,  1,  2,  9, 10, 11, 18, 19, 20], [ 3,  4,  5, 12, 13, 14, 21, 22, 23], [ 6,  7,  8, 15, 16, 17, 24, 25, 26],
            [27, 28, 29, 36, 37, 38, 45, 46, 47], [30, 31, 32, 39, 40, 41, 48, 49, 50], [33, 34, 35, 42, 43, 44, 51, 52, 53],
            [54, 55, 56, 63, 64, 65, 72, 73, 74], [57, 58, 59, 66, 67, 68, 75, 76, 77], [60, 61, 62, 69, 70, 71, 78, 79, 80]]
        
        for (let i = 0; i < this.cells.length; i++) {
            this.cells[i] = new Cell(i, this.cells, 
                this.inArray(i, this.lines), this.inArray(i, this.columns), this.inArray(i, this.squares))
        }

        this.top = this.cells[0]
        this.backtrack = false
        this.last = 80
    }

    inArray(val, arr) {
        return arr.find(subArr => subArr.some(v => v === val))
    }

    next() {
        this.top = (this.top.id === 80 ? undefined : this.cells[this.top.id + 1])
        this.backtrack = false
    }
    prev() {
        this.top = this.cells[this.top.id - 1]
        this.backtrack = true
    }

    resolve() {
        console.log('he ' + this.top)
        if (!this.top) return true
        if (this.top.locked) {
            this.backtrack ? this.prev() : this.next()
            return this.resolve()
        }
        if (this.top.value === 9) {
            this.top.value = 0
            this.prev()
            return false
        }
        this.top.value++
        if (this.top.isLegalValue()) {
            this.next()
            return false
        }
        this.backtrack = true
        return false
    }

    next2(c) {
        this.backtrack = false
        return (c.id === 80 ? undefined : this.cells[c.id + 1])
    }
    prev2(c) {
        this.backtrack = true
        return this.cells[c.id - 1]
    }


    resolveCell(c) {
        if (!c) return undefined
        if (c.locked) {
            return this.backtrack ? this.prev2(c) : this.next2(c)
        }
        if (c.value === 9) {
            c.value = 0
            return this.prev2(c)
        }
        c.value++
        if (c.isLegalValue()) {
            return this.next2(c)
        }
        this.backtrack = true
        return c
    }
}

class Cell {
    constructor(id, cells, line, column, square) {
        this.id = id
        this.grid = cells
        this.line = line
        this.column = column
        this.square = square
        this.value = 0
        this.locked = false
    }

    isLegalValue(val = this.value) {
        return (
               this.line.every(i => this.id === i || this.grid[i].value !== val)
            && this.column.every(i => this.id === i || this.grid[i].value !== val)
            && this.square.every(i => this.id === i || this.grid[i].value !== val)
        )
    }

    set(newVal) {
        this.locked = Boolean(newVal)
        this.value = newVal
    }

    nextLegalValue() {
        if (this.value === 9) return 0
        for (let x = this.value + 1; x <= 9; x++) {
            if (this.isLegalValue(x)) return x
        }
        throw 'Fail of nextLegalValue on cell ' + this.id + ' with value ' + this.value
    }
}

export { Resolver, Cell }
