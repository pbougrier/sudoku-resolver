import React, { Component } from 'react';
import './App.css';
import { Resolver } from './Sudoku'

class App extends Component {
  constructor(props) {
    super(props)
    this.state = {speed: 0}
    this.handleSpeed = this.handleSpeed.bind(this)
  }
  handleSpeed(newSpeed) {
    this.setState({speed: newSpeed})
  }
  render() {
    return (
      <div className="grille">
        <ToolBar onSpeedChange={this.handleSpeed}/>
        <SudokuGrid speed={this.state.speed}/>
      </div>
    )
  }
}

class SudokuGrid extends Component {

  constructor(props) {
    super(props)
    this.initSudoku()
    this.handleCellUpdate = this.handleCellUpdate.bind(this)
    this.componentWillReceiveProps(this.props)
  }

  initSudoku() {
    this.grid = new Resolver()
    this.state = {currentCell: this.grid.cells[0]}
  }

  componentWillReceiveProps(nextProps) {
    if (this.done) return
    if (this.speed === nextProps.speed) return
    if (this.timer) clearInterval(this.timer)
    if (nextProps.speed === -1) this.initSudoku()
    this.speed = nextProps.speed
    console.log("Speed " + this.speed)
    if (this.speed === 0 || this.speed === -1) return
    this.timer = setInterval(() => {
      if (this.state.currentCell) {
        this.setState({currentCell: this.grid.resolveCell(this.state.currentCell)})
      }
    }, this.speed)
  }

  handleCellUpdate(cellId) {
    const c = this.grid.cells[cellId]
    c.set(c.nextLegalValue())
    this.setState({currentCell: this.state.currentCell})
  }

  renderLine(startId, lineStyle = '') {
    return [0, 1, 2, 3, 4, 5, 6, 7, 8].map(i => {
      const style = ' ' + lineStyle + ' ' + (i % 3 === 0 ? 'left' : (i === 8 ? 'right' : ''))
      const id = startId + i
      const c = this.grid.cells[id]
      return (<Cell style={style} key={id} cellId={id} val={c.value} locked={c.locked} onUpdate={() => this.handleCellUpdate(id)}/>)
    })
  }

  render() {
    return (
      <div>
        <div className="ligne">{this.renderLine(0,'top')}</div>
        <div className="ligne">{this.renderLine(9)}</div>
        <div className="ligne">{this.renderLine(18)}</div>
        <div className="ligne">{this.renderLine(27, 'top')}</div>
        <div className="ligne">{this.renderLine(36)}</div>
        <div className="ligne">{this.renderLine(45)}</div>
        <div className="ligne">{this.renderLine(54, 'top')}</div>
        <div className="ligne">{this.renderLine(63)}</div>
        <div className="ligne">{this.renderLine(72, 'down')}</div>
      </div>
    )
  }
  
  componentWillUnmount() {
    clearInterval(this.timer)
  }
}

class ToolBar extends Component {
  constructor(props) {
    super(props)
    this.speeds = [500, 250, 100, 50, 10, 5]
    this.speed = 2
    this.state = {pause: true}
  }
  render() {
    let pauseIcon = this.state.pause ? (
      <div className="cmd" onClick={()=>this.handleSpeed('pause')}>&#x23F8;</div>
    ) : (
      <div className="cmd" onClick={()=>this.handleSpeed('pause')}>&#x23EF;</div>
    )
    return (
      <div className="ligne">
        <div className="cmd" onClick={()=>this.handleSpeed('slower')}>&#x23EA;</div>
        {pauseIcon}
        <div className="cmd" onClick={()=>this.handleSpeed('faster')}>&#x23E9;</div>
        <div className="cmd" onClick={()=>this.handleSpeed('reload')}>&#x23CF;</div>
      </div>
    )
  }
  handleSpeed(command) {
    if (command === 'pause') {
      const nextState = !this.state.pause
      this.setState({pause: nextState})
      this.props.onSpeedChange(nextState ? 0 : this.speeds[this.speed])
    } else if (command === 'slower' && this.speed !== 0) {
      this.props.onSpeedChange(this.speeds[--this.speed])
      this.setState({pause: false})
    } else if (command === 'faster' && this.speed !== (this.speeds.length - 1)) {
      this.props.onSpeedChange(this.speeds[++this.speed])
      this.setState({pause: false})
    } else if (command === 'reload') {
      this.props.onSpeedChange(-1)
      this.setState({pause: true})
    } else {
      throw "Command not supported " + command
    }
  }
}

function Cell(props) {
  return (
    <div className={'case' + (props.locked ? ' red' : '') + props.style} onClick={props.onUpdate}>
      {props.val === 0 ? '' : props.val}
    </div>
  )
}

export { App, ToolBar }
